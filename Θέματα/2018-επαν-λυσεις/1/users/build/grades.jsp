<%@ page import="java.sql.*" %>
<html>
<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
</style>
<head>
	<title>Grades</title>
</head>
<body>
	<table style="width:100%">
		<tr>
			<th>Name</th>
			<th>Grade</th>
		</tr>
		<%
		try (Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/test", "root", "")) {
			String nameUpd = request.getParameter("name");
			String gradeUpd = request.getParameter("grade");
			if (nameUpd != null && gradeUpd != null && nameUpd.trim().length() > 0 && gradeUpd.trim().length() == 1) {
				Statement stmt = con.createStatement();
				String sql = "UPDATE students SET grade=\"" + gradeUpd + "\" WHERE name=\"" + nameUpd + "\";";
				stmt.executeUpdate(sql);
			}

			Statement stmt = con.createStatement();
			String sql = "SELECT * FROM students;";
			for (ResultSet rs = stmt.executeQuery(sql); rs.next(); ) {
				%>
				<tr>
					<%
					String name = rs.getString("name");
					%>
					<td><%= name %></td>
					<%
					String grade = rs.getString("grade");
					if (grade == null) {
						%>
						<td><a href="addGrade.jsp?name=<%= name %>">Add grade</a></td>
					<%
					} else {
					%>
						<td><%= grade %></td>
					<%
					}
					%>
				</tr>
				<%
			}
		}
		%>
	</table>
</body>
</html>
