<%@ page import="java.sql.*" %>
<html>
<head>
	<title>Vote</title>
</head>
<body>
	<%
	try (Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/polls", "root", "")) {
		Statement stmt = con.createStatement();

		String user = request.getParameter("user");
		String sql = "SELECT * FROM participation WHERE username=\"" + user + "\";";
		ResultSet rs = stmt.executeQuery(sql);
		if (rs.next()) {
			// user has already voted
			String vote = rs.getString("voted");
			%>
			<h3>User <%= user %> has already voted "<%= vote %>"</h3>
		<%
		} else {
			String newVote = request.getParameter("vote");
			if (newVote != null) {
				// user just submitted their vote
				String sqlInsert = "INSERT INTO participation (username, voted) VALUES (\"" + user + "\", \"" + newVote + "\");";
				stmt.executeUpdate(sqlInsert);
				%>
				<h3>User <%= user %> has already voted "<%= newVote %>"</h3>
			<%
			} else {
				// user has not voted yet
				%>
				<form>
					<input type="hidden" name="user" value="<%= user %>">
					<input type="radio" name="vote" value="yes">Yes<br>
					<input type="radio" name="vote" value="no">No<br>
					<input type="submit" value="Vote">
				</form>
			<%
			}
		}

		String sqlYes = "SELECT * FROM participation WHERE voted=\"yes\";";
		String sqlNo = "SELECT * FROM participation WHERE voted=\"no\";";
		int votes = 0;
		for (rs = stmt.executeQuery(sqlYes); rs.next(); votes++)
			;
		for (rs = stmt.executeQuery(sqlNo); rs.next(); votes--)
			;
		%>
		Overall result: <%= votes %>
		<%
	}
	%>
</body>
</html>
