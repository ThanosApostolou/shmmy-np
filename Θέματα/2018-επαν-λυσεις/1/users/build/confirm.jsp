<%@ page import="java.sql.*" %>
<html>
<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
</style>
<head>
	<title>Confirm</title>
</head>
<body>
	<%
	try (Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/bank", "root", "")) {
		Statement stmt = con.createStatement();

		if (request.getParameter("finalize") != null) {
			String sqlTemp = "SELECT * FROM temp;";
			for (ResultSet rs = stmt.executeQuery(sqlTemp); rs.next(); ) {
				String id = rs.getString("id");
				String from = rs.getString("from");
				String to = rs.getString("to");
				String amount = rs.getString("amount");
				String sqlUpdFinal = "INSERT INTO final (id, `from`, `to`, amount) VALUES (\""
					+ id + "\", \"" + from + "\", \"" + to + "\", " + amount + ");";
				stmt.executeUpdate(sqlUpdFinal);
			}
			String sqlUpdTemp = "DELETE FROM temp;";
			stmt.executeUpdate(sqlUpdTemp);
		}
		%>
		<h3>Temorary:</h3>
		<table style="width:100%">
			<tr>
				<th>Id</th>
				<th>From</th>
				<th>To</th>
				<th>Amount</th>
			</tr>
			<%
			String sqlTemp = "SELECT * FROM temp;";
			for (ResultSet rs = stmt.executeQuery(sqlTemp); rs.next(); ) {
				String id = rs.getString("id");
				String from = rs.getString("from");
				String to = rs.getString("to");
				String amount = rs.getString("amount");
				%>
				<tr>
					<td><%= id %></td>
					<td><%= from %></td>
					<td><%= to %></td>
					<td><%= amount %></td>
				</tr>
			<%
			}
			%>
		</table>
		<br>
		<h3>Final:</h3>
		<table style="width:100%">
			<tr>
				<th>Id</th>
				<th>From</th>
				<th>To</th>
				<th>Amount</th>
			</tr>
			<%
			String sqlFinal = "SELECT * FROM final;";
			for (ResultSet rs = stmt.executeQuery(sqlFinal); rs.next(); ) {
				String id = rs.getString("id");
				String from = rs.getString("from");
				String to = rs.getString("to");
				String amount = rs.getString("amount");
				%>
				<tr>
					<td><%= id %></td>
					<td><%= from %></td>
					<td><%= to %></td>
					<td><%= amount %></td>
				</tr>
			<%
			}
			%>
		</table>
		<br>
		<form>
			<input type="hidden" name="finalize">
			<input type="submit" value="Confirm Transactions">
		</form>
	<%
	}
	%>
</body>
</html>
