<%@ page import="java.sql.*" %>
<html>
<head>
	<title>Admin</title>
</head>
<body>
	<h3>New poll</h3>
	<form method="post" action="home.jsp">
		<%
		try (Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/polls", "root", "")) {
			Statement stmt = con.createStatement();
			String sql = "SELECT text FROM poll;";
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			String text = rs.getString("text");
			%>
			<textarea name="text" rows=5 cols=50><%= text %></textarea><br>
			<%
		}
		%>
		<input type="submit" value="Submit">
	</form>
</body>
</html>
