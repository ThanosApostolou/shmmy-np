<%@ page import="java.sql.*" %>
<html>
<head>
	<title>Title</title>
	<script>
	function checkit() {
		var s = document.getElementById("number").value;
		var n = Number(s);
		if (isNaN(n)) {
			var error = document.getElementById("error");
			error.innerHTML = "\"" + s + "\" is not a number";
	 		return false;
		} else {
			document.getElementById("number").value = String(n);
	 		return true;
		}
	}
	</script
</head>
<body>
	<%
	String num = request.getParameter("number");
	if (num != null) {
		try (Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/calc", "root", "")) {
			Statement stmt = con.createStatement();
			String sql = "INSERT INTO test (number) VALUES (\"" + num + "\");";
			stmt.executeUpdate(sql);
		}
	}
	%>
	<h3 id="error"></h3>
	<form onsubmit="return checkit();">
		Type a number: <input type="text" name="number" id="number"><br>
		<input type="submit" value="Submit">
	</form>
</body>
</html>
