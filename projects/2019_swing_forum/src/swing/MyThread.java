package swing;

import java.awt.Color;

public class MyThread extends Thread {

	public final Canvas canvas;
	
	public int x;
	public int y;
	public int rad;
	public int msec;
	public Color color;
	public boolean stop;
	public int col;

	public MyThread(Canvas canvas, int x, int y, int s) {
		this.stop = false;
		this.color = Color.red;
		this.canvas = canvas;
		this.x = x;
		this.y = y;
		this.msec = s;
		this.rad = 4;
		this.col = x / 4;
		this.setDaemon(true);
	}
	public void run(){
		while (true){
			try {
				// System.out.println("going to sleep for " + msec);
				sleep(msec);
			} catch (InterruptedException e) {
				System.out.println("sleep interrupted");
				
				e.printStackTrace();
			}
			// System.out.println("woke up");
			if (this.y + this.rad < 500 && !this.stop) {
				this.y += 4;
				canvas.repaint();
			}
			if (this.y + this.rad >= 500) {
				this.stop = true;
			}
		}
	}
}
