package swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import java.util.ArrayList;


public class Canvas extends JLabel{

	private static final long serialVersionUID = 1L;
	public ArrayList<MyThread> threads = new ArrayList<>();



	public Canvas() {
		// Dimension (width, height)
		setPreferredSize(new Dimension(500,500));
	}

	public void addThread(MyThread t) {
		t.start();
		threads.add(t);
	}

	public void paint(Graphics g){
		// System.out.println(threads.size());
		check();
		for (MyThread t : threads) {
			g.setColor(t.color);
			g.fillRect(t.x, t.y, t.rad, t.rad);
		}

	}

	public void check() {
		for (int i = 0; i < threads.size(); i++) {
			for (int j = 0; j < threads.size(); j++) {
				if (i==j || threads.get(i).col != threads.get(j).col) continue;
				int y1 = threads.get(i).y;
				int y2 = threads.get(j).y;
				boolean down = threads.get(j).stop;
				if (y1 == y2 - 4 && down) {
					threads.get(i).stop = true;
				}
			}
		}
	}

	public void reset(){
		threads = null;
	}
}
