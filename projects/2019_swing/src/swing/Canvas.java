package swing;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;;

public class Canvas extends JLabel {
	private static final long serialVersionUID = 1L;
	ArrayList<Shape> shapes = new ArrayList<>();

	int width = 500;
	int height = 500;
	int columns = height / 4;
	int max=0;
	int[] depth = new int[columns];
	

	Canvas() {
		setPreferredSize(new Dimension(width,height));
		for (int i=0; i < depth.length; i++) {
			depth[i]=height;
		}
	}

	public void paint(Graphics g){
		// System.out.println(threads.size());
		//check();
		for (Shape s : shapes) {
			g.setColor(s.color);
			g.fillRect(s.x, s.y, s.rad, s.rad);
		}
	}

	public void reset(){
		shapes = null;
	}
}
