package swing;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class MyThread extends Thread {

	public final Canvas canvas;
	Shape s;
	int msec = 100;

	public MyThread(Canvas canvas, Shape s) {
		this.canvas = canvas;
		this.s = s;
		this.setDaemon(true);
	}
	public void run(){
		while (s.y < s.max){
			try {
				// System.out.println("going to sleep for " + msec);
				sleep(msec);
			} catch (InterruptedException e) {
				System.out.println("sleep interrupted");
				e.printStackTrace();
			}
			// System.out.println("woke up");
			if (s.y+4 > s.max) {
				s.y = s.max;
			} else {
				s.y += 4;
			}
			canvas.repaint();
		}
		
		if (canvas.height - s.max > canvas.max ) {
			canvas.max = canvas.height - s.max;
		}
	}
}
