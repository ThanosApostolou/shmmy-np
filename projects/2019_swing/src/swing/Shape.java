package swing;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Shape {
	private static final AtomicInteger count = new AtomicInteger(0);
	final int id;
	int rad=4;
	Color color = Color.red;
	int x=0;
	int y=0;
	int max;
	
	public Shape (int x, int y, int max) {
		this.id = count.incrementAndGet();
		this.x = x;
		this.y = y;
		this.max = max;
	}
}