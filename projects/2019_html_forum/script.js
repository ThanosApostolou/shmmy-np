$(document).ready(function(){
	const player = 'X';
	const com = 'Y';
	let clicked = 0;
	let xm = 0;
	let ym = 0;
	let board = new Array(25);
	
	for (let i = 0; i < 5; i++) {
		for (let j = 0; j < 5; j++) {
			$("#game-board").append('<button type="button" class="btn" id="' + i + '_' + j +'"></button>')
			board[i*5 + j] = false;
		}
		$("#game-board").append("<br>")
	}
	$(".btn").on("click", function(){
		if ($(this).html() == '') {
			$(this).html(player)
			clicked++
		
			check()
		
			do {
				var row = Math.floor(Math.random() * 5);
				var col = Math.floor(Math.random() * 5);
				console.log(row, col, $('#'+row+'_'+col).html())
			} while ($('#'+row+'_'+col).html() != '' && clicked != 25)
			$('#'+row+'_'+col).html(com)
			clicked++
			
			check()
		}
	})
	
	function check() {
		for (let i = 0; i < 4; i++) {
			console.log(i)
			for (let j = 0; j < 5; j++) {
				const a = $('#'+i+'_'+j).html()
				const b = $('#'+(i+1)+'_'+j).html()
				console.log(a,b)
				let x
				if (a == b && a != '' && !board[5*i+j]) {
					board[5*i+j] = true;
					if (a == 'X') {
						xm++
						c = 'blue'
						$("#x").html('Player X: ' + xm)
					}
					else {
						ym++
						c = 'red'
						$("#y").html('Player Y: ' + ym)
					}
				}
			}
		}
	}
})