package swing;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.MouseInputListener;

import java.util.*;;

public class Canvas extends JLabel implements MouseListener {
	private static final long serialVersionUID = 1L;
	ArrayList<Shape> shapes = new ArrayList<>();

	int width = 500;
	int height = 500;
	int msec=1000;
	

	Canvas() {
		setPreferredSize(new Dimension(width,height));
		addMouseListener(this);
	}

	public void paint(Graphics g){
		// System.out.println(threads.size());
		//check();
		for (Shape s : shapes) {
			g.setColor(s.color);
			g.drawOval(s.x, s.y, 2*s.rad, 2*s.rad);
		}
	}

	public void reset(){
		shapes = null;
	}
	
	public void mouseClicked(MouseEvent e) {
		int x=e.getX();
		int y=e.getY();
		Shape newShape = new Shape (x,y);
		shapes.add(newShape);
		MyThread t = new MyThread(this, newShape, msec);
		t.start();
		repaint();
		msec += 1000;
	}

	public void mouseEntered(MouseEvent e) {
	}
	public void mouseExited(MouseEvent e) {
	}
	public void mousePressed(MouseEvent e) {
	}
	public void mouseReleased(MouseEvent e) {
	}
}
