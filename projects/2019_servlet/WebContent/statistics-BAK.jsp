<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="ISO-8859-7"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.sql.*"%>
<%
	String connectionURL = "jdbc:mysql://localhost:3306/exams2016";
	Connection connection = null;
	Statement statement = null;	
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-7">
<title>Statistics</title>
</head>
<body>
	<%		
	Class.forName("com.mysql.jdbc.Driver").newInstance();
	connection = DriverManager.getConnection(connectionURL, "root", "");
	statement = connection.createStatement();
	String sql = "SELECT grade, COUNT(*)FROM students GROUP BY grade;";
	ResultSet rs;
	rs = statement.executeQuery(sql);
	if (rs == null)
		System.out.println("wtf");
	String a = "0";
	String b = "0";
	String c = "0";
	String d = "0";
	while (rs.next()) {
		String cur = rs.getString("grade");
		String num = rs.getString("COUNT(*)");
		if (cur == null)
			continue;
		if (rs.getString("grade").trim().equals("A")) {
			a = num;
		}
		else if (rs.getString("grade").trim().equals("B")) {
			b = num;
		}
		else if (rs.getString("grade").trim().equals("C")) {
			c = num;
		}
		else if (rs.getString("grade").trim().equals("D")) {
			d = num;
		}
	}
	
	%>			
	<h2> Number of students per grade </h2>
	<table border="5">
	<tr>	
		<td>Grade A:</td>
		<td><%=a %></td>	
	</tr>
	<tr>	
		<td>Grade B:</td>
		<td><%=b %></td>	
	</tr>
	<tr>	
		<td>Grade C:</td>
		<td><%=c %></td>	
	</tr>
	<tr>	
		<td>Grade D:</td>
		<td><%=d %></td>		
	</tr>
	</table>
</body>
</html>