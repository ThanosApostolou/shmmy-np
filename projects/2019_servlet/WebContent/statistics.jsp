<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.sql.*"%>
<%
	String connectionURL = "jdbc:mysql://localhost:3306/exams2016";
	Connection connection = null;
	Statement statement = null;
	ResultSet rs = null;
%>
<html>
<head>
	<title>Statistics</title>
</head>
<body>
	<%		
	Class.forName("com.mysql.jdbc.Driver").newInstance();
	connection = DriverManager.getConnection(connectionURL, "root", "");
	statement = connection.createStatement();
	String sql = "SELECT * FROM students;";
	rs = statement.executeQuery(sql);
	if (rs == null)
		System.out.println("wtf");
	int a = 0;
	int b = 0;
	int c = 0;
	int d = 0;
	while (rs.next()) {
		String cur = rs.getString("grade");
		System.out.println(cur);
		if (cur == null)
			continue;
		if (cur.trim().equals("A")) {
			a++;
		} else if (cur.trim().equals("B")) {
			b++;
		} else if (cur.trim().equals("C")) {
			c++;
		} else if (cur.trim().equals("D")) {
			d++;
		}
	}
	%>			
	<h2> Number of students per grade </h2>
	<table border="5">
	<tr>	
		<td>Grade A:</td>
		<td><%=a %></td>	
	</tr>
	<tr>	
		<td>Grade B:</td>
		<td><%=b %></td>	
	</tr>
	<tr>	
		<td>Grade C:</td>
		<td><%=c %></td>	
	</tr>
	<tr>	
		<td>Grade D:</td>
		<td><%=d %></td>		
	</tr>
	</table>
</body>
</html>