package iprog2;

import java.awt.Color;
import java.awt.Point;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.MouseInfo;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.PointerInfo;

import javax.swing.JLabel;
import java.util.ArrayList;

public class Canvas extends JLabel implements MouseMotionListener{
	
	private static final long serialVersionUID = 1L;
	private int x = -1;
	private int y = -1;
	private int rad = 5;
	public static int curBig = -1;
	private boolean fromMouse = false;
	private ArrayList<Dot> arr = new ArrayList<Dot>();
	
	public Canvas(){
		//System.out.println("Hi apo canvas");
		setPreferredSize(new Dimension(500, 500));
		addMouseMotionListener(this);
	}
	
	public void paint(Graphics g)
	{
		g.setColor(Color.red);
		//System.out.println("Hi apo canvas paint");
		//g.drawOval(50, 50, 2*rad, 2*rad);
		if (curBig >= arr.size())
			curBig = arr.size() - 1;
		if(fromMouse) {
			Dot cur = new Dot(this.x, this.y);
			if (curBig == -1) {
				new MyThread(this).start();
				curBig = 0;
			}
			arr.add(cur);
			for (int i = 0; i < arr.size(); i++) {
				if (i == curBig)
					g.fillOval(arr.get(i).getX(), arr.get(i).getY(), 2*10, 2*10);
				else
					g.fillOval(arr.get(i).getX(), arr.get(i).getY(), 2*rad, 2*rad);
			}
			fromMouse = false;
		}
		else { 
			for (int i = 0; i < arr.size(); i++) {
				if (i == curBig)
					g.fillOval(arr.get(i).getX(), arr.get(i).getY(), 2*10, 2*10);
				else
					g.fillOval(arr.get(i).getX(), arr.get(i).getY(), 2*rad, 2*rad);		
			}
		}
	}



	@Override
	public void mouseDragged(MouseEvent e) {
		
		//Point pnt = e.getLocationOnScreen();
		//Point pnt = MouseInfo.getPointerInfo().getLocation();
		this.x = (int) e.getX();
		this.y = (int) e.getY();
		//System.out.println("Opa mes sto keyevent" + x + " " + y);
		fromMouse = true;
		this.repaint();
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
