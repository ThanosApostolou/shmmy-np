package iprog2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Color;

public class MyFrame extends JFrame  {

	Canvas canvas = new Canvas();
	
	public Canvas getCanvas()
	{
		return canvas;
	}
	
	public void setCanvas(Canvas canvas){
		this.canvas=canvas;
	}
	public static void main(String[] args){
		new MyFrame();
	}
	
	public MyFrame() throws HeadlessException {
		super("Thema 2 - Eksetasi");
		setVisible(true);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		getContentPane().add(canvas);
	    getContentPane().setBackground(Color.WHITE);
		
		//new MyThread(canvas).start();
		pack();
		
		
		}
		
	}

