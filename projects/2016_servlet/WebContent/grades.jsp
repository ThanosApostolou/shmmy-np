<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.sql.*"%>
<%
	String connectionURL = "jdbc:mysql://localhost:3306/exams2016";
	Connection connection = null;
	Statement statement = null;	
	ResultSet rs = null;
%>
<html>
<head>
	<title>Grades</title>
</head>
<body>
	<%
	Class.forName("com.mysql.jdbc.Driver").newInstance();
	connection = DriverManager.getConnection(connectionURL, "root", "");
	statement = connection.createStatement();
	rs = statement.executeQuery("SELECT * FROM students");
	%>
	
	<center>
	<h1> Grades</h1>
	<br/>
	<br/>		
	<table border="5">
		<tr>
			<th>Name</td>
			<th>Grade</td>
			<th>Add Grade</td>
		</tr>
		<%
		while (rs.next()) {
			%>
			<tr>
				<td><%=rs.getString("name")%></td>
				<%
				if  ((rs.getString("grade") == null) || ((rs.getString("grade").trim().equals("")))) {
					session.setAttribute( "nametoaddgrade",rs.getString("name") );
				%>
					<td></td>
					<td><a href="addGrade.jsp?nametoadd=<%= rs.getString("name") %>">Add</a></td>
				<%
				}
				else {
					%>
					<td><%=rs.getString("grade")%></td>
			</tr>
			<%
				}
		}
		rs.close();
	%>
	</table>
	</center>
</body>
</html>