

/*
	 Milonic DHTML Website Navigation Menu
	 Written by Andy Woolley - Copyright 2003 (c) Milonic Solutions Limited. All Rights Reserved
	 Please visit http://www.milonic.co.uk/ for more information

	 Although this software may have been freely downloaded, you must obtain a license before using it in any production environment
	 The free use of this menu is only available to Non-Profit, Educational & Personal Web Sites who have obtained a license to use 
	 
	 Free, Commercial and Corporate Licenses are available from our website at http://www.milonic.co.uk/menu/supportcontracts.php
	 You also need to include a link back to http://www.milonic.co.uk/ if you use the free license
	 
	 All Copyright notices MUST remain in place at ALL times
	 If you cannot comply with all of the above requirements, please contact us to arrange a license waiver
*/



//The following line is critical for menu operation, and MUST APPEAR ONLY ONCE.
menunum=0;menus=new Array();_d=document;function addmenu(){menunum++;menus[menunum]=menu;}function dumpmenus(){mt="<scr"+"ipt language=JavaScript>";for(a=1;a<menus.length;a++){mt+=" menu"+a+"=menus["+a+"];"}mt+="<\/scr"+"ipt>";_d.write(mt)}
//Please leave the above line intact. The above also needs to be enabled if it not already enabled unless you have more than one _array.js file


////////////////////////////////////
// Editable properties START here //
////////////////////////////////////

timegap=500                   // The time delay for menus to remain visible
followspeed=5                 // Follow Scrolling speed
followrate=50                 // Follow Scrolling Rate
suboffset_top=5               // Sub menu offset Top position
suboffset_left=10             // Sub menu offset Left position



XPMainStyle=[                 // XPMainStyle is an array of properties. You can have as many property arrays as you need
"000000",                     // Mouse Off Font Color
"EFEDDE",                     // Mouse Off Background Color (use zero for transparent in Netscape 6)
"000000",                     // Mouse On Font Color
"C1D2EE",                     // Mouse On Background Color
"8A867A",                     // Menu Border Color
"12",                         // Font Size (default is px but you can specify mm, pt or a percentage)
"normal",                     // Font Style (italic or normal)
"normal",                     // Font Weight (bold or normal)
"Tahoma,Helvetica,Verdana",   // Font Name
3,                            // Menu Item Padding or spacing
,                             // Sub Menu Image (Leave this blank if not needed)
0,                            // 3D Border & Separator bar
,                             // 3D High Color
,                             // 3D Low Color
,                             // Current Page Item Font Color (leave this blank to disable)
,                             // Current Page Item Background Color (leave this blank to disable)
,                             // Top Bar image (Leave this blank to disable)
,                             // Menu Header Font Color (Leave blank if headers are not needed)
,                             // Menu Header Background Color (Leave blank if headers are not needed)
,                             // Menu Item Separator Color
]


XPMenuStyle=[                 // XPMenuStyle is an array of properties. You can have as many property arrays as you need
"000000",                     // Mouse Off Font Color
"transparent",                // Mouse Off Background Color (use zero for transparent in Netscape 6)
"000000",                     // Mouse On Font Color
"C1D2EE",                     // Mouse On Background Color
"8A867A",                     // Menu Border Color
"12",                         // Font Size (default is px but you can specify mm, pt or a percentage)
"normal",                     // Font Style (italic or normal)
"normal",                     // Font Weight (bold or normal)
"Tahoma,Helvetica,Verdana",   // Font Name
2,                            // Menu Item Padding or spacing
,                             // Sub Menu Image (Leave this blank if not needed)
0,                            // 3D Border & Separator bar
,                             // 3D High Color
,                             // 3D Low Color
,                             // Current Page Item Font Color (leave this blank to disable)
,                             // Current Page Item Background Color (leave this blank to disable)
,                             // Top Bar image (Leave this blank to disable)
,                             // Menu Header Font Color (Leave blank if headers are not needed)
,                             // Menu Header Background Color (Leave blank if headers are not needed)
,                             // Menu Item Separator Color
]


addmenu(menu=[
"XPMain",                     // Menu Name - This is needed in order for this menu to be called
160,                          // Menu Top - The Top position of this menu in pixels
20,                           // Menu Left - The Left position of this menu in pixels
,                             // Menu Width - Menus width in pixels
0,                            // Menu Border Width
,                             // Screen Position - here you can use "center;left;right;middle;top;bottom" or a combination of "center:middle"
XPMainStyle,                  // Properties Array - this array is declared higher up as you can see above
1,                            // Always Visible - allows this menu item to be visible at all time (1=on or 0=off)
,                             // Alignment - sets this menu elements text alignment, values valid here are: left, right or center
,                             // Filter - Text variable for setting transitional effects on menu activation - see above for more info
0,                            // Follow Scrolling Top Position - Tells this menu to follow the user down the screen on scroll placing the menu at the value specified.
1,                            // Horizontal Menu - Tells this menu to display horizontaly instead of top to bottom style (1=on or 0=off)
0,                            // Keep Alive - Keeps the menu visible until the user moves over another menu or clicks elsewhere on the page (1=on or 0=off)
,                             // Position of TOP sub image left:center:right
3,                            // Set the Overall Width of Horizontal Menu to specified width or 100% and height to a specified amount
0,                            // Right To Left - Used in Hebrew for example. (1=on or 0=off)
0,                            // Open the Menus OnClick - leave blank for OnMouseover (1=on or 0=off)
,                             // ID of the div you want to hide on MouseOver (useful for hiding form elements)
"winxp_back.gif",// Background image for menu Color must be set to transparent for this to work
0,                            // Scrollable Menu
,                             // Miscellaneous Menu Properties
,"File","show-menu=Filemenu","# onbordercolor=316AC5;offbordercolor=EFEDDE;",,0
,"Samples","# offbackcolor=EFEDDE;onbordercolor=316AC5;offbordercolor=EFEDDE;",,,0
,"Legal","show-menu=Legal","# offbackcolor=EFEDDE;onbordercolor=316AC5;offbordercolor=EFEDDE;",,0
,"Colors","show-menu=Colors","# offbackcolor=EFEDDE;onbordercolor=316AC5;offbordercolor=EFEDDE;",,0
])


addmenu(menu=[
"Filemenu",
,
,
160,
1,
,
XPMenuStyle,
0,
,
"fade(duration=0.4);Shadow(color=aaaaaa,Direction=115,Strength=4)",
0,
0,
0,
,
,
0,
0,
,
"winxp_back.gif",
0,
"margin=2;",
,"<img src=xpwidth.gif border=0>Menu Home Page","/ onbordercolor=316AC5;offbordercolor=FFFFFF;",,,0
,"<img src=xpblank.gif border=0>XP Menu Sample","menusample.php?sampleid=25 onbordercolor=316AC5;offbordercolor=FFFFFF;",,,0
,"<img src=xpprint.gif border=0>Print This Page","javascript:self.print() onbordercolor=316AC5;offbordercolor=FFFFFF;",,"Print This Page",0
,"<img src=xpsearch.gif border=0>Search","http://www.google.com/ target=_blank;onbordercolor=316AC5;offbordercolor=FFFFFF;",,,0
,"<img src=xpsearch.gif border=0>Other Links","show-menu=Links","# onbordercolor=316AC5;offbordercolor=FFFFFF;",,0
])


addmenu(menu=[
"Legal",
,
,
140,
1,
,
XPMenuStyle,
0,
,
"fade(duration=0.4);Shadow(color=aaaaaa,Direction=115,Strength=4)",
0,
0,
0,
,
,
0,
0,
,
"winxp_back.gif",
0,
"margin=2;",
,"<img src=xppreview.gif border=0>License Agreement","license.php onbordercolor=316AC5;offbordercolor=FFFFFF;",,,0
,"<img src=xppreview.gif border=0>Privacy Policy","privacy.php onbordercolor=316AC5;offbordercolor=FFFFFF;",,,0
])


addmenu(menu=[
"Colors",
,
,
130,
1,
,
XPMenuStyle,
0,
,
"fade(duration=0.4);Shadow(color=aaaaaa,Direction=115,Strength=4)",
0,
0,
0,
,
,
0,
0,
,
"winxp_back.gif",
0,
"margin=2;",
,"<img src=xpblank.gif border=0>Sample 1","# onbordercolor=316AC5;offbordercolor=FFFFFF;",,,0
,"<img src=xpblank.gif border=0>Sample 2","# onbackcolor=C1BEEE;onbordercolor=5786D5;offbordercolor=FFFFFF;",,,0
,"<img src=xpblank.gif border=0>Sample 3","# onbackcolor=C1AAEE;onbordercolor=AB9CDD;offbordercolor=FFFFFF;",,,0
,"<img src=xpblank.gif border=0>Sample 4","# onbackcolor=C196EE;onbordercolor=8B9CDD;offbordercolor=FFFFFF;",,,0
,"<img src=xpblank.gif border=0>Sample 5","# onbackcolor=C182EE;onbordercolor=A4BDE8;offbordercolor=FFFFFF;",,,0
])


addmenu(menu=[
"Links",
,
,
100,
1,
,
XPMenuStyle,
0,
,
,
0,
0,
0,
,
,
0,
0,
,
"winxp_back.gif",
0,
"margin=2;",
,"<img src=xpwidth.gif border=0>Public Forum","/menu/forum/ onbordercolor=316AC5;offbordercolor=ffffff;",,,0
,"<img src=xpquestion.gif border=0>Menu FAQ","/menu/faq.php onbordercolor=316AC5;offbordercolor=FFFFFF;",,,0
,"<img src=xpblank.gif border=0>Sample 1","# onbordercolor=316AC5;offbordercolor=ffffff;",,,0
,"<img src=xpblank.gif border=0>Sample 1","# onbordercolor=316AC5;offbordercolor=ffffff;",,,0
,"<img src=xpblank.gif border=0>Sample 1","# onbordercolor=316AC5;offbordercolor=ffffff;",,,0
])




dumpmenus();
	