/*  Random number generator. If max=3 then function returns 1,2 or 3
===================================================================*/
function getRandom(max) {return (Math.floor(Math.random()*max))+1;}



/*  Determine depth
===================================================================*/
     if (DepthOfPage == 0) { depth = ""; }
else if (DepthOfPage == 1) { depth = "../"; }
else if (DepthOfPage == 2) { depth = "../../"; }
else if (DepthOfPage == 3) { depth = "../../../"; }
else if (DepthOfPage == 4) { depth = "../../../../"; }
else if (DepthOfPage == 5) { depth = "../../../../../"; }
else if (DepthOfPage == 6) { depth = "../../../../../../"; }



/*  Bookmarker
===================================================================*/
if (navigator.appName == "Microsoft Internet Explorer") {
   bookmarkline = "<A CLASS=\"sidebarb2\" HREF=\"javascript:window.external.AddFavorite(location.href,'pagetutor.com')\">Add this page to your Favorites</A>";
} else {
   bookmarkline = "<A CLASS=\"sidebarb2\" HREF=\"javascript:alert('Press CTRL+D to bookmark this page.')\">Bookmark this page</A>";
}



/*  Large banner ads
===================================================================*/
//online-  TopAdNumber = getRandom(1000); TopBannerAd = "<A HREF=\"http://kansas.valueclick.com/redirect?host=hs0164664&b=" + TopAdNumber + "&v=0\" TARGET=\"_top\"><IMG BORDER=\"0\" WIDTH=\"468\" HEIGHT=\"60\" ALT=\"Please visit our sponsors.\" SRC=\"http://kansas.valueclick.com/cycle?host=hs0164664&b=" + TopAdNumber + "&noscript=1\"></A>";
//online-  BtmAdNumber = getRandom(1000); BtmBannerAd = "<A HREF=\"http://kansas.valueclick.com/redirect?host=hs0164664&b=" + BtmAdNumber + "&v=0\" TARGET=\"_top\"><IMG BORDER=\"0\" WIDTH=\"468\" HEIGHT=\"60\" ALT=\"Please visit our sponsors.\" SRC=\"http://kansas.valueclick.com/cycle?host=hs0164664&b=" + BtmAdNumber + "&noscript=1\"></A>";

// kills large banner ads
// TopBannerAd = "<IMG SRC=\"" + depth + "tack/adpics/test01.gif\" WIDTH=\"468\" HEIGHT=\"60\" BORDER=\"0\">";
// BtmBannerAd = "<IMG SRC=\"" + depth + "tack/adpics/test01.gif\" WIDTH=\"468\" HEIGHT=\"60\" BORDER=\"0\">";


//offline-
AdLine = new Array();
AdLine[1]  = "<A HREF=\"http://service.bfast.com/bfast/click?bfmid=12768212&siteid=22315569&bfpage=home\" TARGET=\"_top\"><IMG SRC=\"" + depth + "tack/adpics/nomonthlyfees_468x60_" + getRandom(2) + ".gif\" BORDER=\"0\" WIDTH=\"468\" HEIGHT=\"60\" ALT=\"www.NoMonthlyFees.com\"></A>";
AdLine[2]  = "<a href=\"http://www.commission-junction.com/track/track.dll?AID=14617&PID=517001&URL=http%3A%2F%2Fwww%2Eanimfactory%2Ecom\" target=\"_top\"><img src=\"" + depth + "tack/adpics/animationfactory_468x60_" + getRandom(2) + ".gif\" width=\"468\" height=\"60\" alt=\"Free Animations\" border=\"0\"></a>";
AdLine[3]  = "<a href=\"http://www.commission-junction.com/track/track.dll?AID=446071&PID=517001&URL=http%3A%2F%2Fwww%2Etv\" target=\"_top\"><img src=\"" + depth + "tack/adpics/dottv_468x60_" + getRandom(2) + ".gif\" width=\"468\" height=\"60\" alt=\"What's the Coolest Domain?\" border=\"0\"></a>";
AdLine[4]  = "<A HREF=\"http://triton.whitelakeweb.com/~tina/cgi-bin/refid.cgi?id=webtutor\" TARGET=\"_top\"><IMG SRC=\"" + depth + "tack/adpics/affordablehost_468x60_" + getRandom(2) + ".gif\" WIDTH=\"468\" HEIGHT=\"60\" BORDER=\"0\" ALT=\"Click here for affordable hosting.\"></A>";
AdLine[5]  = "<A HREF=\"http://www.notetab.com/pwd.htm\" TARGET=\"_top\"><IMG SRC=\"" + depth + "tack/adpics/notetab_468x60_1.gif\" WIDTH=\"468\" HEIGHT=\"60\" BORDER=\"0\" ALT=\"Try an award winning text editor.\"></A>";
AdLine[6]  = "<A HREF=\"http://www.marketingtips.com/t.cgi/46811/\" TARGET=\"_top\"><IMG SRC=\"" + depth + "tack/adpics/marketingtips_468x60_" + getRandom(3) + ".gif\" WIDTH=\"468\" HEIGHT=\"60\" BORDER=\"0\" ALT=\"Click here to visit our sponsor.\"></A>";
AdLine[7]  = "<A HREF=\"http://www.amazon.com/exec/obidos/redirect-home/professionaweb02\" TARGET=\"_top\"><IMG SRC=\"" + depth + "tack/adpics/amazonbooks_468x60_" + getRandom(2) + ".gif\" WIDTH=\"468\" HEIGHT=\"60\" BORDER=\"0\" ALT=\"Click here to visit our sponsor.\"></A>";
AdLine[8]  = "<A HREF=\"http://www.amazon.com/exec/obidos/redirect-home/professionaweb02\" TARGET=\"_top\"><IMG SRC=\"" + depth + "tack/adpics/amazonmusic_468x60_" + getRandom(2) + ".gif\" WIDTH=\"468\" HEIGHT=\"60\" BORDER=\"0\" ALT=\"Click here to visit our sponsor.\"></A>";
AdLine[9]  = "<a href=\"http://www.commission-junction.com/track/track.dll?AID=12817&PID=517001&URL=http%3A%2F%2Fwww%2Einternetcharges%2Ecom\" target=\"_top\"><img src=\"" + depth + "tack/adpics/internetcharges_468x60_" + getRandom(3) + ".gif\" width=\"468\" height=\"60\" alt=\"Credit card processing made easy\" border=\"0\"></a>";
AdLine[10] = "<a href=\"http://www.commission-junction.com/track/track.dll?AID=10346&PID=517001&URL=http%3A%2F%2Fwww%2Ecj%2Ecom%2FAffiliate%2Findex%2Easp\" target=\"_top\"><img src=\"" + depth + "tack/adpics/commissionjunction_468x60_" + getRandom(3) + ".gif\" width=\"468\" height=\"60\" alt=\"Commission Junction - Get Paid!\" border=\"0\"></a>";
AdLine[11] = "<a href=\"http://www.commission-junction.com/track/track.dll?AID=512361&PID=517001&URL=http%3A%2F%2Fwww%2Egifart%2Ecom\" target=\"_top\"><img src=\"" + depth + "tack/adpics/gifart_468x60_" + getRandom(2) + ".gif\" width=\"468\" height=\"60\" alt=\"Clipart And Web Ready Graphics.\" border=\"0\"></a>";

TopBannerAd = AdLine[getRandom(11)];
BtmBannerAd = AdLine[getRandom(11)];


