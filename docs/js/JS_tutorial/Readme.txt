====================================================================
Javascript Tutor  v1.02 - free version                 by Joe Barta
http://www.pagetutor.com/javascript/                  pagetutor.com
====================================================================

This is the self contained version of Javascript Tutor. It is
freeware and freely distributable. Info for shareware sites below.

DESCRIPTION: Javascript Tutor is a basic, yet intensive set of 25
lessons covering basic javascript programming. The student will get
a firm grounding in a number of concepts and be ready to move on to
more ambitious projects. This tutorial assumes no previous programming
experience and contains many hands-on excercises designed to
reinforce the topics covered. The free version contains the first
several lessons. The full version is available with a membership and
contains 25 brain stretching lessons.

To use: Unzip into a new folder and start with index.html.

Have fun & good luck!

Joe Barta
 jbarta@pagetutor.com
  http://www.pagetutor.com/
====================================================================


VERSION HISTORY:

v1.02
Divided into free version and full version.

v1.01
Fixed link problem in Lesson 19

v1.0 
First public release.
====================================================================


INFO FOR SHAREWARE SITES:

NAME:     Javascript Tutor
STATUS:   Freeware
VERSION:  1.02
SIZE:     500 Kb

RELEASE DATE: September 15, 2000

APPLICATION HOMEPAGE:
http://www.pagetutor.com/javascript/

DOWNLOAD:
http://www.pagetutor.com/javascript/jstutor.zip
ftp://ftp.simtel.net/pub/simtelnet/win95/html/jstut102.zip

DESCRIPTION: (short)
Javascript Tutor: Learn basic javascript

DESCRIPTION: (longer)
Javascript Tutor is a basic, yet intensive set of 25 lessons
covering basic javascript programming. The student will get
a firm grounding in a number of concepts and be ready to
move on to more ambitious projects. This free tutorial assumes
no previous programming experience and contains many hands-on
excercises designed to reinforce the topics covered.

SCREEN CAPTURE: http://www.pagetutor.com/cap_jstutor.gif
  ICON (32x32): http://www.pagetutor.com/icon_jstutor.gif

CHANGES:   Update

AUTHOR:    Joe Barta - pagetutor.com
HOMEPAGE:  http://www.pagetutor.com/
EMAIL:     jbarta@pagetutor.com

SPECIAL REQUIREMENTS: None
DISTRIBUTION: This file is freely distributable.
====================================================================
